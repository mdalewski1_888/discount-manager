package wh.md.discount;

import java.util.List;

import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;
import wh.md.discount.domain.TotalPrice;

public interface PriceCalculator {

    void setDiscountRules(List<DiscountRule> discountRules);

    TotalPrice calculatePrice(List<Product> products);
}
