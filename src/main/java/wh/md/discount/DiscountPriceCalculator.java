package wh.md.discount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;
import wh.md.discount.domain.TotalPrice;
import wh.md.discount.handlers.DiscountRuleHandler;

public class DiscountPriceCalculator implements PriceCalculator {

    private List<DiscountRule> discountRules = new ArrayList<>();
    private DiscountRuleHandlerFactory discountRuleHandlerFactory = new DiscountRuleHandlerFactory();

    @Override
    public void setDiscountRules(List<DiscountRule> providedDiscountRules) {
        this.discountRules = providedDiscountRules;
    }

    @Override
    public TotalPrice calculatePrice(List<Product> products) {
        BigDecimal totalPrice = products.stream()
                                        .map(p -> calculatePriceForSingleProduct(p))
                                        .reduce(BigDecimal.ZERO, BigDecimal::add);
        return new TotalPrice(totalPrice);
    }

    private BigDecimal calculatePriceForSingleProduct(Product product) {
        Optional<DiscountRule> productDiscountRule = discountRules.stream()
                                                                  .filter(discountRule -> product.getName().equals(discountRule.getProductName()))
                                                                  .findFirst();

        if (!productDiscountRule.isPresent()) {
            return product.getBasicPrice().multiply(BigDecimal.valueOf(product.getCount()));
        } else {
            DiscountRuleHandler discountRualeHandler = discountRuleHandlerFactory.getDiscountRualeHandler(productDiscountRule.get().getRuleType());
            return discountRualeHandler.apply(product, productDiscountRule.get());
        }
    }
}
