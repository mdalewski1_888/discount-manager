package wh.md.discount.handlers;

import java.math.BigDecimal;

import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;

public interface DiscountRuleHandler {

    BigDecimal apply(Product product, DiscountRule productDiscountRule);
}
