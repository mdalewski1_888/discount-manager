package wh.md.discount.handlers;

import java.math.BigDecimal;

import wh.md.discount.domain.DiscountRule;
import wh.md.discount.domain.Product;
import wh.md.discount.handlers.DiscountRuleHandler;

public class SingleProductDiscountHandler implements DiscountRuleHandler {

    @Override
    public BigDecimal apply(Product product, DiscountRule productDiscountRule) {
        BigDecimal basicTotalPrice = product.getBasicPrice().multiply(BigDecimal.valueOf(product.getCount()));
        if (product.getCount() >= productDiscountRule.getRequiredCount()) {
            return basicTotalPrice.subtract(product.getBasicPrice().subtract(productDiscountRule.getDiscountedPrice()));
        } else {
            return basicTotalPrice;
        }
    }
}
